## Arduino Libraries Bratc

Libraries for arduino

## Robotic
Libraries for robotic education

## Example

```
#include <ButtonToggle.h>
ButtonToggle buttonTog(3);
void setup()
{
  pinMode(13, OUTPUT);
}

void loop(){
  while(buttonTog.buttonToggle()){
    parpadear();
  }
}

void parpadear(){
  digitalWrite(13,HIGH);
  delay(200);
  digitalWrite(13,LOW);
  delay(100);
  
}
```

## Licence GNU 
