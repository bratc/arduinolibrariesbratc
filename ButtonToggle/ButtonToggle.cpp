
/**
* 
* This library allows you to create a Button Toggle the Sensor Touch module in any arduino pin.
*
* The ButtonToggle library is based on the code of David A. Mellis 200.
*
* By Yohon Jairo Bravo Castro
* yjairobravo@gmail.com
*/

#include "Arduino.h"
#include "ButtonToggle.h"

ButtonToggle::ButtonToggle(int pin){
  pinMode(pin,INPUT);
  _pin = pin;
}

boolean ButtonToggle::buttonToggle(){
  
  reading = digitalRead(_pin);

  // if the input just went from LOW and HIGH and we've waited long enough
  // to ignore any noise on the circuit, toggle the output pin and remember
  // the time
  
  if (reading == HIGH && previous == LOW && millis() - time > debounce) {
    if(status == true){
      status = false;
    }else {
      status = true;
    }
    
    time = millis();    
  }
  previous = reading;
  return status;
}
