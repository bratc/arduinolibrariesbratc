#ifndef ButtonToggle_h
#define ButtonToggle_h

#include "Arduino.h"

class ButtonToggle {
  public:
    ButtonToggle(int pin);
    boolean buttonToggle();
  private:
    int _pin; 
    int reading;           // the current reading from the input pin
    int previous = LOW;    // the previous reading from the input pin

// the follow variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
    long time = 0;         // the last time the output pin was toggled
    long debounce = 200;   // the debounce time, increase if the output flickers
    boolean status = false; 
};
#endif
